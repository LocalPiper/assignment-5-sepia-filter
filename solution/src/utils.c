#include "../include/utils.h"
#include <stdio.h>
#include <unistd.h>
void print_error(const char *message) {
  fputs(message, stderr);
  fputs("\n", stderr);
}
int file_exists(const char *filename) { return access(filename, F_OK) == 0; }
