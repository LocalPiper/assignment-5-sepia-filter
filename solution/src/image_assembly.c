#include "../include/image_assembly.h"
#include "../include/bmp_img.h"
#include "../include/image.h"
#include "../include/padding.h"
#include "../include/read_status.h"
#include "../include/utils.h"
#include <stdint.h>
#include <stdlib.h>

struct image *copy_image(const struct image *source) {
  struct image *copy = create_blank_image(source->width, source->height);
  for (uint32_t i = 0; i < (copy->width * copy->height); ++i) {
    copy->data[i].b = source->data[i].b;
    copy->data[i].g = source->data[i].g;
    copy->data[i].r = source->data[i].r;
  }
  return copy;
}

struct pixel *generate_empty_pixels(struct pixel *storage, const uint32_t width,
                                    const uint32_t height) {
  for (uint32_t i = 0; i < (height * width); ++i) {
    storage[i].b = 0;
    storage[i].g = 0;
    storage[i].r = 0;
  }
  return storage;
}

struct image *create_blank_image(const uint32_t width, const uint32_t height) {
  struct image *empty_image = (struct image *)malloc(sizeof(struct image));
  if (!empty_image) {
    print_error("OUT OF MEMORY: CAN'T CREATE IMAGE");
    return NULL;
  }
  empty_image->width = width;
  empty_image->height = height;
  empty_image->data =
      (struct pixel *)malloc(height * width * sizeof(struct pixel));
  if (!empty_image->data) {
    print_error("OUT OF MEMORY: CAN'T CREATE DATA FOR IMAGE");
  }
  empty_image->data = generate_empty_pixels(empty_image->data, width, height);
  return empty_image;
}
