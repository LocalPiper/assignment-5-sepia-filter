#include "../include/file_write.h"
#include "../include/image_disassembly.h"
#include "../include/utils.h"
#include <stdio.h>

enum write_status to_bmp(const char *filename, struct image **source) {
  FILE *f = fopen(filename, "wb");
  if (f == NULL) {
    print_error("Error while opening file to write");
    return WRITE_OPEN_ERROR;
  }

  write_image(f, source);

  if (fclose(f) != 0) {
    print_error("Error while closing file to write");
    return WRITE_CLOSE_ERROR;
  }

  return WRITE_OK;
}
