#include "../include/padding.h"
#include "../include/pixel.h"
#define BMP_ROW_PADDING 4
uint8_t calculate_padding(uint32_t width) {
  return (BMP_ROW_PADDING - width * sizeof(struct pixel) % BMP_ROW_PADDING) %
         BMP_ROW_PADDING;
}
