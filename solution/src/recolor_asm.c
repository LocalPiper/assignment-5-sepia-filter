#include "../include/recolor_asm.h"

void sepia_asm(struct image **source) {
  recolor_asm_impl((*source)->width, (*source)->height, (*source)->data);
}
