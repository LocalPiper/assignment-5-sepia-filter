section .rodata
align 16
f_line: dd 0.131, 0.168, 0.189
align 16
s_line: dd 0.534, 0.686, 0.769
align 16
t_line: dd 0.272, 0.349, 0.393

section .text
; rdi - width
; rsi - height
; rdx - pointer to original pixel array
global recolor_asm_impl
recolor_asm_impl:
    ; get data from struct image
    xor rax, rax
    
    mov r10, rdi
    mov r11, rsi
    
    xor r8, r8  ; counter height
    .loop_height:
        xor r9, r9  ; counter width
        .loop_width:
            pxor xmm0, xmm0
            pxor xmm1, xmm1
            pxor xmm2, xmm2    
            
            ; save blue to reg
            pxor mm0, mm0
            xor rax, rax
            mov al, [rdx]
            movq mm0, rax  
            cvtpi2ps xmm0, mm0   
            pshufd xmm0, xmm0, 0x00
            xor rax, rax
            movq rax, xmm0
            
            ; save green to reg
            pxor mm0, mm0
            xor rax, rax
            mov al, [rdx+1]
            movq mm0, rax
            cvtpi2ps xmm1, mm0
            pshufd xmm1, xmm1, 0x00
            xor rax, rax
            movq rax, xmm1
            
            ; save red to reg
            pxor mm0, mm0
            xor rax, rax
            mov al, [rdx+2]
            movq mm0, rax
            cvtpi2ps xmm2, mm0
            pshufd xmm2, xmm2, 0x00
            xor rax, rax
            movq rax, xmm2
            
            ; amplify colors for sepia effect
            mulps xmm0, [f_line]
            mulps xmm1, [s_line]
            mulps xmm2, [t_line]
            
            ; summ to get resulting colors
            addps xmm0, xmm1
            addps xmm0, xmm2
            
            cvtps2dq xmm0, xmm0
            
            ; save new colors
            movd eax, xmm0
            mov [rdx], al
            pshufd xmm0, xmm0, 0xe5
            movd eax, xmm0
            mov [rdx+1], al
            pshufd xmm0, xmm0, 0xe6
            movd eax, xmm0
            mov [rdx+2], al
            
            add rdx, 3
            inc r9
            cmp r9, rdi
            js .loop_width
        inc r8
        cmp r8, rsi
        js .loop_height   
    ret
