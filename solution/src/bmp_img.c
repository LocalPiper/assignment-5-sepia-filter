#include "../include/bmp_img.h"
struct bmp_header create_header(const struct image *source,
                                const uint8_t padding) {
  struct bmp_header header = {
      header.bfType = SIGNATURE,
      header.bfileSize =
          sizeof(struct bmp_header) +
          sizeof(struct pixel) * (source->width + padding) * source->height,
      header.bfReserved = RESERVED,
      header.bOffBits = OFF_BITS,
      header.biSize = SIZE,
      header.biWidth = source->width,
      header.biHeight = source->height,
      header.biPlanes = PLANES,
      header.biBitCount = BIT_COUNT,
      header.biCompression = COMPRESSION_TYPE,
      header.biSizeImage = (source->width + padding) * source->height,
      header.biXPelsPerMeter = X_PELS_PER_METER,
      header.biYPelsPerMeter = Y_PELS_PER_METER,
      header.biClrUsed = CLR_USED,
      header.biClrImportant = CLR_IMPORTANT};
  return header;
}
