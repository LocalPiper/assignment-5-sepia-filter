#include "../include/recolor.h"
#include "../include/image.h"
#include <stdint.h>
#define MAX_COLOR_VALUE 255

float get_new_color(struct pixel *pixel, const float r_amplifier,
                    const float g_amplifier, const float b_amplifier) {
  return pixel->r * r_amplifier + pixel->g * g_amplifier +
         pixel->b * b_amplifier;
}

uint8_t normalize_color(float color) {
  if (color > MAX_COLOR_VALUE) {
    return (uint8_t)MAX_COLOR_VALUE;
  }
  return (uint8_t)color;
}

void recolor_to_sepia(struct pixel *pixel) {
  float float_sepia_red = get_new_color(pixel, 0.393, 0.769, 0.189);
  float float_sepia_green = get_new_color(pixel, 0.349, 0.686, 0.168);
  float float_sepia_blue = get_new_color(pixel, 0.272, 0.534, 0.131);

  pixel->r = normalize_color(float_sepia_red);
  pixel->g = normalize_color(float_sepia_green);
  pixel->b = normalize_color(float_sepia_blue);
}
void sepia(struct image **source) {
  for (int i = 0; i <= (*source)->height * (*source)->width; ++i) {
    recolor_to_sepia(&((*source)->data[i]));
  }
}
