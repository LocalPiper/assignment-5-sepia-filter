#include "../include/test.h"
#include "../include/image_assembly.h"
#include "../include/recolor.h"
#include "../include/recolor_asm.h"
#include <stdlib.h>
#include <time.h>
float test_c(const struct image *source) {
  float time = 0;
  for (size_t i = 0; i < 100; ++i) {
    struct image *copy = copy_image(source);
    clock_t start = clock();
    sepia(&copy);
    clock_t end = clock();
    time += ((float)end - (float)start);
    free(copy->data);
  }
  return time;
}

float test_asm(const struct image *source) {
  float time = 0;
  for (size_t i = 0; i < 100; ++i) {
    struct image *copy = copy_image(source);
    clock_t start = clock();
    sepia_asm(&copy);
    clock_t end = clock();
    time += ((float)end - (float)start);
    free(copy->data);
  }
  return time;
}
