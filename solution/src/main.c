#include "../include/file_read.h"
#include "../include/image_assembly.h"
#include "../include/recolor.h"
#include "../include/recolor_asm.h"
#include "../include/test.h"
#include "../include/utils.h"
#include "../include/validate.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  // check if args are valid
  if (validate(argc, argv) == 1) {
    puts("\nProgram terminated.");
    return 1; // Return error code
  }
  puts("CONTENTS LOADED");
  const char *source_filename = argv[1];
  const char *destination_c_filename = "c_out.bmp";
  const char *destination_asm_filename = "asm_out.bmp";

  // get image object from file
  struct image *image_c = NULL;
  if (from_bmp(source_filename, &image_c) != READ_OK) {
    return 1;
  }
  puts("GOT IMAGE");
  struct image *image_asm = copy_image(image_c);
  puts("IMAGE COPIED");
  // recolor image
  float c_time = test_c(image_c);
  float asm_time = test_asm(image_asm);
  puts("Time for 100 tries:");
  printf("C: %f\n", c_time);
  printf("ASM: %f\n", asm_time);
  printf("Difference: %f\n", c_time / asm_time);
  free(image_c->data);
  free(image_asm->data);
  puts("DONE");
  // and we are good to go
  return 0;
}
