#pragma once
#include "image.h"
#include <stdint.h>
extern struct image *create_blank_image(uint32_t width, uint32_t height);
extern struct image *copy_image(const struct image *source);
