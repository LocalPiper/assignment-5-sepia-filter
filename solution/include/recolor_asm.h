#include "image.h"
#include <stdint.h>
extern void sepia_asm(struct image **source);
void recolor_asm_impl(const uint32_t width, const uint32_t height,
                      struct pixel *pixels);
