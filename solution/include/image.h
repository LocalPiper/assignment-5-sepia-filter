#pragma once
#include "pixel.h"
#include <stdint.h>
struct image {
  uint32_t width, height;
  struct pixel *data;
};
