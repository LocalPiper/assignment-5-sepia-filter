enum write_status {
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_OPEN_ERROR,
  WRITE_CLOSE_ERROR
};
