CC = gcc
ASM = nasm
ASMFLAGS = -felf64
CFLAGS =
LDFLAGS = -no-pie
SRCFILES = $(wildcard ./solution/src/*c)
MKDIR = mkdir -p
RM = rm -rf
MV = mv
BUILDDIR = build
OBJDIR = obj
SOLDIR = solution
TARGET = sepia_filter

all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(MKDIR) $(OBJDIR)/$(SOLDIR)
	$(MKDIR) $(BUILDDIR)
	$(CC) -O2 -c $(SRCFILES)
	$(MV) *.o $(OBJDIR)/$(SOLDIR)
	$(ASM) $(ASMFLAGS) $(SOLDIR)/src/recolor_asm_impl.asm -o $(OBJDIR)/$(SOLDIR)/recolor_asm_impl.o
	$(CC) -O2 $(LDFLAGS) $(OBJDIR)/$(SOLDIR)/*.o -o $(TARGET)
clean:
	$(RM) $(BUILDDIR)/$(TARGET)
	$(RM) $(OBJDIR)
